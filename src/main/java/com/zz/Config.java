package com.zz;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import com.zz.utils.Constant;
import com.zz.utils.Util;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Created by benshanyue on 2017/5/2.
 */
@Configuration
@EnableCaching
public class Config {
	/**
	 * 控制上传文件大小
	 *
	 * @return
	 * @author Limeng'en
	 * @date 2016年9月23日
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("1024MB");
		factory.setMaxRequestSize("1024MB");
		return factory.createMultipartConfig();
	}

	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver clr = new CookieLocaleResolver();
		clr.setCookieMaxAge(30 * 24 * 60 * 60);
		clr.setCookieName("lang");
		return clr;
	}

	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		return filter;
	}

	@Bean(name = "appEhCacheCacheManager")
	public EhCacheCacheManager ehCacheCacheManager(EhCacheManagerFactoryBean bean) {
		return new EhCacheCacheManager(bean.getObject());
	}

	@Bean
	public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
		EhCacheManagerFactoryBean cacheManagerFactoryBean = new EhCacheManagerFactoryBean();
		cacheManagerFactoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
		cacheManagerFactoryBean.setShared(true);
		return cacheManagerFactoryBean;
	}

	@Bean
	public Filter authFilter() {
		return new Filter() {

			@Override
			public void init(FilterConfig arg0) throws ServletException {
			}

			@Override
			public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain)
					throws IOException, ServletException {
				HttpServletRequest request = (HttpServletRequest) arg0;
				HttpServletResponse response = (HttpServletResponse) arg1;
				String requestURI = request.getRequestURI();
				if (requestURI != null
						&& requestURI.matches("^((/css/)|(/ueditor/)|(/js/)|(/img/)|(/font/)|(/upload/)|(/html/)).*")) {
					chain.doFilter(arg0, arg1);
					return;
				}
				HttpSession session = request.getSession();
				if (requestURI != null && requestURI.indexOf("/system") != -1
						&& request.getHeader("X-Requested-With") == null) {
					// 若session 中无用户，直接踢出到登录页
					if (Util.isEmpty(session.getAttribute(Constant.LOGIN_USER))) {
						session.setAttribute(Constant.GLOBAL_URL, requestURI);// 保存用户踢出前路径。
						response.sendRedirect(Constant.LOGIN_PAGE);// 跳转登录页
						return;
					}
				}
				// 如果在登录页 ,且已登录，就直接跳到该角色首页
				else if (requestURI != null && requestURI.indexOf("/login") != -1
						&& request.getHeader("X-Requested-With") == null) {
					if (!Util.isEmpty(session.getAttribute(Constant.LOGIN_USER))
							&& !Util.isEmpty(session.getAttribute(Constant.LOGIN_USER_ROLE))) {
						response.sendRedirect(Constant.PAGE_ADMIN);// 跳转后台
					}
				}
				chain.doFilter(arg0, arg1);
				return;
			}

			@Override
			public void destroy() {
			}

		};
	}
}
