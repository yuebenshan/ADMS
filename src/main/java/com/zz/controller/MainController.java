package com.zz.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zz.utils.Constant;


@Controller
@RequestMapping("/")
public class MainController {
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public Object main(HttpServletRequest request, HttpServletResponse arg1, HttpSession session, Model model){
		HttpServletResponse response = (HttpServletResponse) arg1;
		try {
			response.sendRedirect(Constant.LOGIN_PAGE);
		} catch (IOException e) {
		}
		return "index";
	}
	
	@RequestMapping(value = "adduser", method = RequestMethod.GET)
	public Object adduser(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		return "reg";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public Object login(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		return "login";
	}
	
	@RequestMapping(value = "error", method = RequestMethod.GET)
	public Object error(){
		return "error";
	}
}
