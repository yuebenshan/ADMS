package com.zz.repository.Userrole;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.zz.repository.user.User;

@Entity
@Table(name = "adms_userrole")
public class Userrole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 权限id
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;
	// 权限cn名字
	@Column(name = "rolename", nullable = false)
	private String rolename;
	// 权限中文名字
	@Column(name = "cnname", nullable = false)
	private String cnname;
	// 权限等级
	@Column(name = "level", nullable = false)
	private Integer memberlevel;
	// 权限下会员列表
	@OneToMany(targetEntity = User.class, cascade = CascadeType.ALL, mappedBy = "role")
	private List<User> user;

	public Integer getRid() {
		return id;
	}

	public void setRid(Integer rid) {
		this.id = rid;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getCnname() {
		return cnname;
	}

	public void setCnname(String cnname) {
		this.cnname = cnname;
	}

	public Integer getMemberlevel() {
		return memberlevel;
	}

	public void setMemberlevel(Integer memberlevel) {
		this.memberlevel = memberlevel;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUserentiy(List<User> user) {
		this.user = user;
	}
}
