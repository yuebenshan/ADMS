package com.zz.repository.Userrole;

import com.zz.repository.BaseRepository;

import java.util.List;

public interface UserroleRepository extends BaseRepository<Userrole, Long>{
    List<Userrole> findByMemberlevelLessThanEqual(Integer memberlevel);
}
