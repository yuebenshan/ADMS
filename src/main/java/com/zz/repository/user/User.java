package com.zz.repository.user;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.zz.repository.Userrole.Userrole;

@Entity
@Table(name = "adms_user")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 用户id
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long id;
	// 用户名
	@Column(name = "username", nullable = false, length = 255)
	private String username;
	// 用户密码
	@Column(name = "password", nullable = false, length = 512)
	private String password;
	// 用户邮箱
	private String email;
	// 用户电话
	private String phone;
	// 用户注册时间
	private Timestamp regtime;
	// 用户最后登录时间
	private Timestamp logintime;
	// 用户余额
	@Column(name = "ubalance", nullable = false, columnDefinition = "FLOAT default 00.00")
	private float ubalance;// 余额
	// 用户微信号
	private String weixinid;
	// 用户权限
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH, targetEntity = Userrole.class)
	@JoinColumn(name = "roleid", referencedColumnName = "id")
	private Userrole role;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Timestamp getRegtime() {
		return regtime;
	}

	public void setRegtime(Timestamp regtime) {
		this.regtime = regtime;
	}

	public Timestamp getLogintime() {
		return logintime;
	}

	public void setLogintime(Timestamp logintime) {
		this.logintime = logintime;
	}

	public float getUbalance() {
		return ubalance;
	}

	public void setUbalance(float ubalance) {
		this.ubalance = ubalance;
	}

	public String getWeixinid() {
		return weixinid;
	}

	public void setWeixinid(String weixinid) {
		this.weixinid = weixinid;
	}

	public Userrole getRole() {
		return role;
	}

	public void setRole(Userrole role) {
		this.role = role;
	}
}
