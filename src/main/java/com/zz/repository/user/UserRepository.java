package com.zz.repository.user;

import java.util.List;

import com.zz.repository.BaseRepository;
import com.zz.repository.Userrole.Userrole;
public interface UserRepository extends BaseRepository<User, Long>{

	User findOneByUsername(String username);

	User findOneByWeixinid(String weixinid);

}
