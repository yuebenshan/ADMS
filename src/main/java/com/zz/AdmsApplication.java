package com.zz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zz.utils.FileHelper;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EntityScan
@EnableJpaRepositories
@EnableTransactionManagement
@SpringBootApplication
public class AdmsApplication {

	public static void main(String[] args) {
		SpringApplication sa = new SpringApplication(AdmsApplication.class);
		sa.addListeners(new ApplicationListener<ApplicationEvent>() {
			@Override
			public void onApplicationEvent(ApplicationEvent applicationEvent) {
				if (applicationEvent instanceof EmbeddedServletContainerInitializedEvent) {
					FileHelper.CONTEXT_DISK_FILE_PATH = ((EmbeddedServletContainerInitializedEvent) applicationEvent).getApplicationContext().getServletContext().getRealPath("/");
					FileHelper.initPath();
				}
			}
		});
		sa.run(args);
	}
}
