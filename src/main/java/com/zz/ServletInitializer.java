package com.zz;

import java.io.IOException;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.zz.utils.FileHelper;

/**
 * Created by benshanyue on 2017/5/2.
 */
public class ServletInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        application.listeners(new ApplicationListener<ApplicationEvent>() {

            @Override
            public void onApplicationEvent(ApplicationEvent event) {
                if (event instanceof ContextRefreshedEvent) {
                	try {
						FileHelper.CONTEXT_DISK_FILE_PATH = ((ContextRefreshedEvent) event)
								.getApplicationContext()
								.getResource("/WEB-INF/classes/static")
								.getFile().getPath();
						FileHelper.initPath();
					} catch (IOException e) {
						e.printStackTrace();
					}
                }
            }

        });
        return application.sources(AdmsApplication.class);
    }
}
