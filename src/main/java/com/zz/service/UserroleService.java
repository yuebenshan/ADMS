package com.zz.service;

import java.util.List;

import com.zz.repository.Userrole.UserroleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.repository.Userrole.Userrole;
import com.zz.repository.user.UserRepository;

@Service
public class UserroleService {
	@Autowired
	private UserroleRepository userroleRepository;

	public List<Userrole> getroles(Userrole role) {
		return userroleRepository.findByMemberlevelLessThanEqual(role.getMemberlevel());
	}

}
