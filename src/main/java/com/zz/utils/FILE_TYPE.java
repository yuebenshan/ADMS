package com.zz.utils;

public enum FILE_TYPE {

	  STORE_LOGO("/store/"), MEDIA_PHOTO("/media/");

	  private String path;

	  FILE_TYPE(String path) {
	    this.path = path;
	  }

	  public String getPath() {
	    return path;
	  }

	  public void setPath(String path) {
	    this.path = path;
	  }

	}