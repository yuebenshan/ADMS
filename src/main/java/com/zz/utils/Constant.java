package com.zz.utils;

public class Constant {
	/** 验证码session. */
	public static final String CAPTCHA = "captcha";

	/** 登录session名称. */
	public static final String LOGIN_USER = "login_user";

	/** 登录用户角色session名称. */
	public static final String LOGIN_USER_ROLE = "login_user_role";

	/** 全局链接，用于页面发现无登录用户，存登录前链接使用。 */
	public static final String GLOBAL_URL = "global_url";

	/** 登录页地址. */
	public static final String LOGIN_PAGE = "/login";

	/** 后台超级权限页地址. */
	public static final String PAGE_ADMIN = "/system";
}
