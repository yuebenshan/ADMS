$(function() {
    $("#adduser").validate({
        rules: {
            username: {
                required: true,
                minlength: 2,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 16
            },
            repassword: {
                required: true,
                equalTo: "#password"
            },
            weixinid: {
                required: true
            }
        },
        messages: {
            username: {
                required: "用户名不能为空",
                minlength: "用户名的最小长度为2"
            },
            password: {
                required: "密码不能为空",
                minlength: "密码长度不能少于6个字符",
                maxlength: "密码长度不能超过16个字符"
            },
            repassword: {
                required: "确认密码不能为空",
                equalTo: "确认密码和密码不一致"
            },
            weixinid: {
                required: "微信账号不能为空"
            }
        }
    });
    $.validator.setDefaults({
        submitHandler: function() {
            $('#adduser').ajaxForm(function(data) {
                alert(data.info);
            });
        }
    });
})
