$(function() {
    $("#login_Form").validate({
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            username: {
                required: "用户名不能为空",
            },
            password: {
                required: "密码不能为空",
            },
        }
    });
    $.validator.setDefaults({
        submitHandler: function() {
            $('#login_Form').ajaxForm(function(data) {
                alert(data.info);
            });
        },
    });
})
