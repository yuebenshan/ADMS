<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>添加用户</title>
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/base.css">
<link rel="stylesheet" href="/css/reg.css">
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.form.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/jquery.validate.extend.js"></script>
<script src="/js/unti.js"></script>
<script src="/js/reg.js"></script>
</head>
<body>
<form id="adduser" action="/data/adduser" method="post">
    <fieldset>
        <legend>jquery-validate表单校验验证</legend>
        <label for="username">用户名：</label><input id="username" name="username" type="text" placeholder="用户名" autocomplete="off" tip="请输入用户名">
        <label for="password">密码：</label><input id="password" name="password" type="password" placeholder="登录密码" tip="长度为6-16个字符">
        <label for="repassword">重复密码：</label><input id="repassword" name="repassword" type="password" placeholder="登录密码" tip="长度为6-16个字符">
        <label for="weixinid">微信公众号：</label><input id="weixinid" name="weixinid" type="text" placeholder="微信公众号" tip="微信公众号">
        <button id="submit">提交</button>
    </fieldset>
</form>
</body>
</html>