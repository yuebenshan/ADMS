<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>系统登录</title>
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/login.css">
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.form.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/jquery.validate.extend.js"></script>
<script src="/js/unti.js"></script>
<script src="/js/login.js"></script>
</head>
<body>
<div class="logo">
    <img src="/img/logo4.png" width="100" height="100" alt=""><span>智能管理系统</span>
</div>
<div class="loginInput">
    <div class="loginBox">
    <form id="login_Form" class="form-inline" action="/data/login" method="post">
        <fieldset>
            <div class="form-group">
                <label for="username">username</label>
                <input id="username" class="form-control" name="username" type="text" placeholder="用户名" autocomplete="off" >
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input id="password" name="password" class="form-control" type="password" placeholder="登录密码" >
              </div>
            <button type="submit" class="btn btn-default">Send invitation</button>
        </fieldset>
    </form>
    </div>
</div>
</body>
</html>